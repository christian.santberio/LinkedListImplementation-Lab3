package linkedLists;
/**
 * Singly linked list with references to its first and its
 * last node. 
 * 
 * @author pirvos
 *
 */

import linkedLists.LinkedList;

public class SLFLList<E> implements LinkedList<E> {

	private SNode<E> first, last;
	int length = 0;

	public SLFLList() {
		first = last = null;
	}

	public void addFirstNode(Node<E> nuevo) {
		((SNode<E>) nuevo).setNext(first);
		first = (SNode<E>) nuevo;
		length++;
	}

	public void addNodeAfter(Node<E> target, Node<E> nuevo) {
		SNode<E> targetNode = (SNode<E>) target;
		SNode<E> newNode = (SNode<E>) nuevo;

		newNode.setNext(targetNode.getNext());
		targetNode.setNext(newNode);
		length++;
	}

	public void addNodeBefore(Node<E> target, Node<E> nuevo) {
		
		Node<E> previousNode = null;
		
		if(target == first)
			this.addFirstNode(nuevo);
		else
			previousNode = getNodePreviousTo(target);
			this.addNodeAfter(previousNode, nuevo);	
	}

	public Node<E> getFirstNode() throws NodeOutOfBoundsException {
		if (first == null) {
			throw new NodeOutOfBoundsException("getFirstNode: list is empty");
		}
		return first;
	}

	public Node<E> getLastNode() throws NodeOutOfBoundsException {
		if (length == 0)
			throw new NodeOutOfBoundsException("getLastNode: list is empty");
		else if(last == null)
			return null;
		
		SNode<E> lastNode = first;
		
		while(lastNode.getNext() != null) {
			lastNode = lastNode.getNext();
		}
	
		return lastNode;
	}

	public Node<E> getNodeAfter(Node<E> target) throws NodeOutOfBoundsException {
		SNode<E> targetNode = (SNode<E>) target;
		if (target == last)
			throw new NodeOutOfBoundsException("getNodeAfter: target Node is the last node.");
		else
			return targetNode.getNext();
	}

	public Node<E> getNodeBefore(Node<E> target) throws NodeOutOfBoundsException {
		if (target == first)
			throw new NodeOutOfBoundsException("getNodeBefore: target is the First Node.");
		else
			return getNodePreviousTo(target);
	}

	public int length() {
		return this.length;
	}

	public void removeNode(Node<E> target) {
		
		if (target == first) {
			
			SNode<E> dummy = first;
			first = first.getNext();
			dummy.setNext(null);
			
		} else {
			SNode<E> previousNode = (SNode<E>) this.getNodeBefore(target);
			SNode<E> nextNode = ((SNode<E>) target).getNext();
			previousNode.setNext(nextNode);
		}
		
		length--;
	}

	public Node<E> createNewNode() {
		return new SNode<E>();
	}
	
	public Node<E> getNodePreviousTo(Node<E> target){
		
		SNode<E> targetNode = (SNode<E>) target;
		if(target == first) {
			return null;
		}else {
			SNode<E> currentNode = first;
			while (currentNode.getNext() != null) {
				currentNode = currentNode.getNext();	
			}		
			return currentNode;
		}
	}

	///////////////////////////////////////////////////
	// private and static inner class that defines the
	// type of node that this list implementation uses
	private static class SNode<T> implements Node<T> {
		private T element;
		private SNode<T> next;

		public SNode() {
			element = null;
			next = null;
		}

		public SNode(T data, SNode<T> next) {
			this.element = data;
			this.next = next;
		}

		public SNode(T data) {
			this.element = data;
			next = null;
		}

		public T getElement() {
			return element;
		}

		public void setElement(T data) {
			this.element = data;
		}

		public SNode<T> getNext() {
			return next;
		}

		public void setNext(SNode<T> next) {
			this.next = next;
		}
	}

}
